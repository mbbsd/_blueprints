from datetime import date
from math import isnan
from re import search, sub
from pandas import DataFrame, ExcelWriter, ExcelFile, read_excel
from pkgs.syllabus import schedule


class academic:

    domicile = 'https://academico.ifgoiano.edu.br/qacademico/professores/'
    username = 'SEU_SIAPE'
    password = 'SUA_SENHA'

    def __init__(self, puppet, payload):
        self.puppet = puppet
        self.payload = payload

        crono = schedule(payload).crono()
        for key in crono.keys():
            payload['mastery'][key]['blueprint'] = crono[key]

    def login(self):
        self.puppet.get(self.domicile)
        self.puppet.find_element_by_id(
                'txtLogin'
                ).send_keys(self.username)
        self.puppet.find_element_by_id(
                'txtSenha'
                ).send_keys(self.password)
        self.puppet.find_element_by_id(
                'btnOk'
                ).click()

    def change_semester(self):
        opening = self.payload['opening']
        self.puppet.find_element_by_name('CMBANO').click()
        self.puppet.find_element_by_xpath(
                '//option[@value="%s"]' % opening.year
                ).click()
        if opening < date(opening.year, 8, 1):
            half = 1
        else:
            half = 2
        self.puppet.find_element_by_name('CMBPERIODO').click()
        self.puppet.find_element_by_xpath(
                '//option[@value="%s"]' % half
                ).click()
        self.puppet.find_element_by_xpath(
                '//input[@name="Atu_AnoPeriodo"]'
                ).click()

    def logbooks(self):
        self.puppet.find_element_by_xpath(
                '//img[@title="Meus Diários"]'
                ).click()
        lgbks = [
                x.text for x in
                self.puppet.find_elements_by_xpath(
                    '//tr[@class="conteudoTexto"]/td/strong'
                    )
                ]
        N = len(lgbks)
        logbooks = {
                lgbks[i]: {'info': lgbks[i+1]} for i in range(N) if i % 2 == 0
                }
        for lgbk in logbooks.keys():
            logbooks[lgbk]['stdt'] = {}
            self.puppet.find_element_by_xpath(
                    '//a[contains(@href, "MODO=FALTAS&COD_PAUTA=%s")]' % lgbk
                    ).click()
            stdts = self.puppet.find_elements_by_xpath(
                    '//a[contains(@href, "COD_MATRICULA")]'
                    )
            skeys = []
            for stdt in stdts:
                skey = search(
                        r'COD_MATRICULA=([0-9]+)',
                        stdt.get_attribute('href')
                        )[1]
                if skey not in skeys:
                    skeys.append(skey)
            for skey in skeys:
                logbooks[lgbk]['stdt'][skey] = {}
                stdt_data = [
                        x.text for x in stdts
                        if search(skey, x.get_attribute('href'))
                            ]
                logbooks[lgbk]['stdt'][skey]['code'] = stdt_data[0]
                logbooks[lgbk]['stdt'][skey]['name'] = stdt_data[1]
            self.puppet.back()
        return logbooks

    def contents(self):
        self.puppet.find_element_by_xpath(
                '//img[@title="Meus Diários"]'
                ).click()
        for key in self.payload['mastery'].keys():
            adjn = self.payload['mastery'][key]['adjoining']
            lgbk = self.payload['mastery'][key]['commonlog']
            self.puppet.find_element_by_xpath(
                    '//a[contains(@href, "MODO=FALTAS&COD_PAUTA=%s")]' % lgbk
                    ).click()
            for span in self.payload['mastery'][key]['blueprint'].keys():
                shot = self.payload['mastery'][key]['blueprint'][span]
                try:
                    span_format = span.strftime('%d%%2F%m%%2F%Y')
                    self.puppet.find_element_by_xpath(
                            '//a[contains(@href, "%s")]' % span_format
                            ).click()
                    # date
                    dclass = self.puppet.find_element_by_name(
                            'DT_AULA_MINISTRADA_EDITAR'
                            )
                    dclass.clear()
                    dclass.send_keys(span.strftime('%d/%m/%Y'))
                    # how many classes
                    nclass = self.puppet.find_element_by_name('N_AULAS')
                    nclass.clear()
                    nclass.send_keys(adjn)
                    # essay
                    effort = self.puppet.find_element_by_name('CONTEUDO')
                    effort.clear()
                    effort.send_keys(shot)
                    self.puppet.find_element_by_xpath(
                            '//input[@id="btnSalvar"]'
                            ).click()
                except ValueError:
                    self.puppet.find_element_by_name(
                            'DT_AULA_MINISTRADA'
                            ).send_keys(span.strftime('%d/%m/%Y'))
                    self.puppet.find_element_by_name(
                            'N_AULAS'
                            ).send_keys(adjn)
                    self.puppet.find_element_by_name(
                            'CONTEUDO'
                            ).send_keys(shot)
                    self.puppet.find_element_by_xpath(
                            '//input[@value="Inserir"]'
                            ).click()
            self.puppet.back()

    def register(self):
        lgbks = self.logbooks()
        with ExcelWriter('brew/register.ods', engine='odf') as ods:
            for key in self.payload['mastery'].keys():
                clkey = self.payload['mastery'][key]['commonlog']
                stdt_df = DataFrame(
                        list(lgbks[clkey]['stdt'].values()),
                        index=list(lgbks[clkey]['stdt'].keys())
                        )
                stdt_df.to_excel(ods, sheet_name='stdt_%s' % key)
                cntnt = {
                        'essay': {
                            k.strftime('%d/%m/%Y'): v for k, v in
                            self.payload['mastery'][key]['blueprint'].items()
                            }
                        }
                for skey in lgbks[clkey]['stdt']:
                    name = lgbks[clkey]['stdt'][skey]['name']
                    cntnt[name] = {k: '' for k in cntnt.keys()}
                cntnt_df = DataFrame(cntnt, index=list(cntnt['essay'].keys()))
                cntnt_df.to_excel(ods, sheet_name='cntnt_%s' % key)

    def odsParser(self):
        lbk = {}
        with ExcelFile('brew/register.ods') as ods:
            df = read_excel(ods, sheet_name=None)
            stdts = [key for key in df.keys() if 'stdt' in key]
            for stdt in stdts:
                dct = df[stdt].to_dict()
                idx = dct['Unnamed: 0'].keys()
                key = sub(r'stdt_', r'', stdt)
                lbkey = self.payload['mastery'][key]['commonlog']
                lbk[lbkey] = {
                        dct['Unnamed: 0'][k]: {
                            'code': dct['code'][k],
                            'name': dct['name'][k],
                            } for k in idx
                        }
                lbk[lbkey]['exam'] = {}
                dts = [
                        key for key in dct.keys()
                        if key not in ['Unnamed: 0', 'code', 'name']
                        ]
                for dt in dts:
                    y, m, d = search(
                            r'([\d]{4})-([\d]{2})-([\d]{2})',
                            str(dt)
                            ).groups()
                    dte = date(int(y), int(m), int(d))
                    lbk[lbkey]['exam'][dte] = dte.strftime('%d/%m/%Y')
                    for k in idx:
                        grd = dct[dt][k]
                        if isnan(grd) is True:
                            lbk[lbkey][dct['Unnamed: 0'][k]][dte] = ''
                        else:
                            lbk[lbkey][dct['Unnamed: 0'][k]][dte] = grd
        return lbk

    def exams(self):
        self.puppet.find_element_by_xpath(
                '//img[@title="Meus Diários"]'
                ).click()
        lbk = self.odsParser()
        for lbkey in lbk.keys():
            self.puppet.find_element_by_xpath(
                    '//a[contains(@href, "COD_PAUTA=%s&ETAPA=1")]' % lbkey
                    ).click()
            c = 1
            for exkey in lbk[lbkey]['exam']:
                self.puppet.find_element_by_xpath(
                        '//input[@value="Inserir"]'
                        ).click()
                self.puppet.find_element_by_xpath(
                        '//input[@name="DESC_AVALIACAO"]'
                        ).send_keys('A%d' % c)
                self.puppet.find_element_by_xpath(
                        '//input[@name="NOTA_MAX"]'
                        ).send_keys('10,0')
                self.puppet.find_element_by_xpath(
                        '//input[@name="DT_AVALIACAO"]'
                        ).send_keys(lbk[lbkey]['exam'][exkey])
                self.puppet.find_element_by_xpath(
                        '//input[@value="Inserir"]'
                        ).click()
                c += 1
            self.puppet.find_element_by_link_text(
                    'Meus Diários'
                    ).click()

    def grades(self):
        self.puppet.find_element_by_xpath(
                '//img[@title="Meus Diários"]'
                ).click()
        lbk = self.odsParser()
        for lbkey in lbk.keys():
            self.puppet.find_element_by_xpath(
                    '//a[contains(@href, "COD_PAUTA=%s&ETAPA=1")]' % lbkey
                    ).click()
            ekeys = list(lbk[lbkey]['exam'].keys())
            N = len(ekeys)
            c = 0
            while c < N:
                link = [
                        search(
                            r'COD_AVALIACAO=([0-9]+)',
                            x.get_attribute('href')
                            )[1]
                        for x in self.puppet.find_elements_by_link_text(
                            'Lançar Notas'
                            )
                        ][c]
                href = 'LANCAR&COD_PAUTA=%s&COD_AVALIACAO=%s' % (lbkey, link)
                self.puppet.find_element_by_xpath(
                        '//a[contains(@href, "%s")]' % href
                        ).click()
                skeys = [k for k in lbk[lbkey].keys() if k != 'exam']
                for skey in skeys:
                    grd = str(lbk[lbkey][skey][ekeys[c]]).replace('.', ',')
                    self.puppet.find_element_by_xpath(
                            '//input[@name="NOTA%s"]' % skey
                            ).send_keys(grd)
                self.puppet.find_element_by_xpath(
                        '//input[@value="Salvar"]'
                        ).click()
                self.puppet.find_element_by_link_text(
                        'Diário %s, Etapa N: Avaliações' % lbkey
                        ).click()
                c += 1
        self.puppet.find_element_by_link_text(
                'Meus Diários'
                ).click()

    def quit(self):
        self.puppet.find_element_by_id('Image1').click()
        self.puppet.quit()
