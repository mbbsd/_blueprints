from datetime import date, timedelta
from os import system
from jinja2 import FileSystemLoader, Environment
from workalendar.america import Brazil


class schedule:

    def __init__(self, payload):
        self.payload = payload

    def semester(self):
        opening = self.payload['opening']
        closing = date(opening.year, 7, 31)
        if opening < closing:
            half = 1
        else:
            half = 2
        semester = '{}/{}'.format(opening.year, half)
        return semester

    def index(self):
        toc_f = 'sphinx/source/index.rst'
        clear_toc_f = '[ -f {} ] && rm {}'.format(toc_f, toc_f)
        system(clear_toc_f)
        print('rm sphinx/source/index.rst')
        t_dir = 'sphinx/source/planos'
        t_dir_clean = 'if [ -d {} ]; then rm -rf {}/*; else mkdir -p {}; fi'
        clear_t_dir = t_dir_clean.format(t_dir, t_dir, t_dir)
        system(clear_t_dir)
        print('rm -rf sphinx/source/planos/*')
        title = 'Disciplinas Ministradas em {}'.format(self.semester())
        uline = ''
        for c in title:
            uline += '='
        header = '{}\n{}\n\n'.format(title, uline)
        w_spcs = ' ' * 3
        toc = ''
        toc += '.. toctree::\n{}'.format(w_spcs)
        toc += ':caption: Planos de Ensino:\n{}'.format(w_spcs)
        toc += ':maxdepth: 1\n\n'
        index = header + toc
        mastery = self.payload['mastery']
        mastery_keys = list(mastery.keys())
        N = len(mastery_keys)
        i = 0
        while i < N-1:
            course = mastery_keys[i]
            index += '{}planos/{}.rst\n'.format(w_spcs, course)
            i += 1
        course = mastery_keys[N-1]
        index += '{}planos/{}.rst'.format(w_spcs, course)
        with open('sphinx/source/index.rst', 'w') as f:
            print(index, file=f)

    def templates(self):
        mastery = self.payload['mastery']
        for course in mastery.keys():
            tmplt_src = '{}.rst'.format(course)
            tmplt_bib = '{}.bib'.format(course)
            tmplt = FileSystemLoader('sphinx/templates')
            envmt = Environment(loader=tmplt)
            template = envmt.get_template(tmplt_src)
            final = template.render(semestre=self.semester())
            n_tmplt = 'sphinx/source/planos/{}'.format(tmplt_src)
            with open(n_tmplt, 'w') as f:
                print(final, file=f)
            cp_bib = 'cp sphinx/templates/{} sphinx/source/planos/'
            system(cp_bib.format(tmplt_bib))

    def grounded(self):
        opening = self.payload['opening']
        grounded = [dt[0] for dt in Brazil(year=opening.year).holidays()]
        for dt in grounded:
            dt_wday = dt.weekday()
            if dt_wday in [1, 3]:
                if dt_wday == 1:
                    y = dt - timedelta(days=1)
                else:
                    y = dt + timedelta(days=1)
                grounded.append(y)
        # 2020 Xmas' reccess
        xmas_opening = date(2020, 12, 14)
        xmas_closing = date(2021, 1, 4)
        while xmas_opening < xmas_closing:
            grounded.append(xmas_opening)
            xmas_opening += timedelta(days=1)
        return grounded

    def crono(self):
        crono = {key: {} for key in self.payload['mastery'].keys()}
        delta = {key: {} for key in self.payload['mastery'].keys()}
        for key in crono.keys():
            c_freq = self.payload['mastery'][key]['frequency']
            c_days = list(c_freq.keys())
            l_days = len(c_days)
            i = 0
            while i < l_days:
                delta[key][c_days[i]] = timedelta(
                        days=(c_days[(i+1) % l_days] - c_days[i]) % 7
                        )
                i += 1
            classday = self.payload['opening']
            while classday.weekday() not in c_freq.keys():
                classday += timedelta(days=1)
            blueprint = self.payload['mastery'][key]['blueprint']
            i = 0
            while i in blueprint.keys():
                if classday in self.grounded():
                    classday += delta[key][classday.weekday()]
                else:
                    q = c_freq[classday.weekday()]
                    while q > 0 and i in blueprint.keys():
                        if classday in crono[key].keys():
                            crono[key][classday] += ', ' + blueprint[i]
                            i += 1
                            q -= 1
                        else:
                            crono[key][classday] = blueprint[i]
                            i += 1
                            q -= 1
                    classday += delta[key][classday.weekday()]
        return crono

    def csv_crono(self):
        crono = self.crono()
        for course in self.payload['mastery'].keys():
            adjoining = self.payload['mastery'][course]['adjoining']
            N = len(self.payload['mastery'][course]['blueprint'].keys())
            dest_file = 'sphinx/source/planos/{}.csv'.format(course)
            with open(dest_file, 'w') as f:
                for dt in crono[course].keys():
                    span = dt.strftime('%d/%m/%Y')
                    shot = crono[course][dt]
                    message = '"{}", {}, "{}"'.format(span, adjoining, shot)
                    print(message, file=f)
                message = 'Total, {}, ---'.format(adjoining * N)
                print(message, file=f)
