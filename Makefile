easy:
	python -m easy

clean:
	if [ -f geckodriver.log ]; then \
		rm geckodriver.log; \
	fi
	if [ -d __pycache__ ]; then \
		rm -rf __pycache__; \
	fi
	rm brew/*

grad:
	python -m grad

hard:
	python -m hard

open:
	firefox sphinx/build/html/index.html &

push:
	git add .
	git commit -a
	git push

venv:
	if [ ! -d venv ]; then \
		python3 -m venv venv; \
		. venv/bin/activate; \
		pip install --upgrade pip; \
		pip install -r requirements.txt; \
		deactivate; \
	fi

.PHONY: easy clean grad hard open push venv
