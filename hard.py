#!/usr/bin/env python3

from os import system
from datetime import date
from pkgs.syllabus import schedule
from pkgs.timeline import blueprint


def main():
    payload = {
            'opening': date(2020, 11, 16),
            'mastery': {
                # 'lo_inf': {
                #     'blueprint': blueprint.lo_inf,
                #     'frequency': {1: 1, 4: 1},
                #     'adjoining': 3,
                #     },
                'nc_eng': {
                    'blueprint': blueprint.nc_eng,
                    'frequency': {0: 1, 2: 1},
                    'adjoining': 3,
                    },
                'ra_mat': {
                    'blueprint': blueprint.ra_mat,
                    'frequency': {2: 2, 4: 1},
                    'adjoining': 3,
                    },
                }
            }

    second_half_2020 = schedule(payload)

    second_half_2020.index()
    second_half_2020.templates()
    second_half_2020.crono()
    second_half_2020.csv_crono()

    system('cd sphinx && make html')


if __name__ == '__main__':
    main()
