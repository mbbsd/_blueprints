Cálculo Numérico
================

Apresentação
------------

+----------------+------------------+----------------+-------------+
| **Curso**      | Engenharia Agrícola                             |
+----------------+------------------+----------------+-------------+
| **Disciplina** | Cálculo Numérico | **Modalidade** | Obrigatória |
+----------------+------------------+----------------+-------------+
| **Código**     | DEG-404          | **Requisito**  |             |
+----------------+------------------+----------------+-------------+
| **Carga**      | **À Distância**  | **Teórica**    | 0h          |
| **Horária**    +------------------+----------------+-------------+
|                | **Presencial**   | **Prática**    | 0h          |
|                |                  +----------------+-------------+
|                |                  | **Teórica**    | 68h         |
+----------------+------------------+----------------+-------------+

Disciplina ministrada por *Marcelo Barboza* em {{ semestre }}.

Ementa
------

Erros. Convergência. Séries de Taylor. Solução numérica de equações
não-lineares. Cálculo numérico de autovalores e autovetores. Interpolação.
Integração numérica. Soluções aproximadas para equações diferenciais ordinárias
e parciais.

Objetivos
---------

O cálculo numérico é o estudo de algoritmos cujo objetivo é a solução numérica
(aproximada) de equações.

Geral
~~~~~

Apresentar aos estudantes técnicas que lhes permitam aproximar funções por
funções elementares, integrar numericamente e, também, obter soluções
aproximadas para certas equações cujos tipos classificamos em algébricas,
não-lineares e diferenciais, ordinárias ou parciais.

Específico
~~~~~~~~~~

Resolver numericamente a sistemas de equações lineares. Aproximar funções por
funções elementares (polinomiais), integrar numericamente. Resolver
numericamente a equações diferenciais ordinárias e parciais.

Programa
--------

#. Erros:

   - conversão de bases;
   - erros de arredondamento e de truncamento;
   - propagação de erros;

#. Sistemas lineares de equações:

   - sistemas triangulares;
   - métodos de substituição retroativa/sucessiva;
   - escalonamento;
   - decomposição lu com pivotação;
   - convergência dos métodos iterativos;

#. Equações não-lineares:

   - métodos:

     * da bisseção;
     * das cordas;
     * pégaso;
     * de Newton;
     * da iteração linear;

   - convergência dos métodos;

#. Interpolação:

   - interpolação:

     * linear;
     * quadrática;
     * de Lagrange;
     * pelo método das diferenças divididas;
     * pelo método das diferenças finitas;

#. Integração numérica:

   - regra dos trapézios;
   - primeira regra de Simpson;
   - segunda regra de Simpson;

#. Solução numérica de EDO's e EDP's lineares de primeira ordem:

   - método das aproximações sucessivas para EDO's;
   - convergência do método das aproximações sucessivas;
   - método das curvas características para EDP's lineares de primeira ordem em
     :math:`\mathbb{R}^2`.

Metodologia
-----------

As aulas terão caráter teórico-expositivo e se darão de forma síncrona através
do Google Meet. O Professor utilizará de mesa-digitalizadora para a explanação
de conteúdos. É essencial que os alunos leiam o livro texto e resolvam o maior
número possível de exercícios.

Avaliação
---------

A avaliação de cada estudante se dará pela percepção do Professor acerca de sua
evolução no decorrer da disciplina. Para tanto, serão avaliadas sua participação
no fórum de discussões do Moodle, nos encontros síncronos via Google Meet e,
finalmente, sua habilidade em resolver aos exercícios propostos.

A nota final :math:`N_f` será assim composta:

#. seja :math:`M` a menção atribuída ao estudante pelo Professor;

#. se :math:`L` a média das notas obtidas pelo estudante em listas de
   exercícios, isto é, se :math:`p` é o número de listas de exercícios no
   decorrer de toda a disciplina, então

   .. math:: L = \dfrac{L_1+\cdots+L_p}{p},

   onde :math:`L_i` representa a nota do aluno em sua lista de excercícios de
   número :math:`i\in\{1,2,\ldots,p\}`;

#. seja :math:`P` a média das notas obtidas pelo estudante em suas duas
   avaliações escritas, ou seja,

   .. math:: P=\dfrac{P_1+P_2}{2},

   onde :math:`P_i` representa a nota do aluno em sua avaliação de número
   :math:`i\in\{1,2\}`,

então:

.. math:: N_f=\dfrac{2M+L+P}{4};

Aprovação por média, reprovação por média e recuperação obedecem, enfim, ao
seguinte critério:

+-----------------------+--------------------------+----------------------+
| Aprovação por média   | Recuperação              | Reprovação por média |
+=======================+==========================+======================+
| :math:`N_f\geqslant6` | :math:`3\leqslant N_f<6` | :math:`N_f<3`        |
+-----------------------+--------------------------+----------------------+

Recuperação
-----------

A cada estudante em recuperação será atribuída uma tarefa cuja execução dependa
de conteúdos aprendidos na disciplina. Se :math:`R` é a nota que corresponde a
este trabalho, então a média final :math:`M_f` do estudante será assim
calculada:

.. math:: M_f=\mbox{max}\left\{N_f,\dfrac{N_f+R}{2}\right\}.

Aprovação e reprovação obedecem ao seguinte critério:

+-----------------------+---------------+
| Aprovação             | Reprovação    |
+=======================+===============+
| :math:`M_f\geqslant6` | :math:`M_f<6` |
+-----------------------+---------------+

Bibliografia
------------

.. bibliography:: ../referencias/nc_eng.bib
   :style: alpha
   :all:

Material Complementar
---------------------

Vídeos do IMPA:

    - `André Nachbin <https://goo.gl/saLmHB>`_;

Páginas da web:

    #. perguntas e respostas:

        - https://math.stackexchange.com/questions/tagged/numerical-methods;
        - https://impa.br/publicacoes;

    #. ferramentas open source de fácil utilização:

        - https://python.org;
        - https://scipy.org;
        - https://anaconda.com.

Formato das aulas
-----------------

+-------------------------+----------------------+
| Atividade               | Tempo médio previsto |
+=========================+======================+
| Leitura do livro texto  | 20 min               |
+-------------------------+----------------------+
| Resolução de exercícios | 20 min               |
+-------------------------+----------------------+
| Vídeos                  | 35 min               |
+-------------------------+----------------------+
| Aula síncrona           | 90 min               |
+-------------------------+----------------------+

Links para as aulas
-------------------

+------+-----+--------------------------------------+
| 2020 | Seg | https://meet.google.com/ytb-gdap-abj |
|      +-----+--------------------------------------+
|      | Qua | https://meet.google.com/hvs-epcq-tpg |
+------+-----+--------------------------------------+
| 2021 | Seg | https://meet.google.com/rko-ggdz-efm |
|      +-----+--------------------------------------+
|      | Qua | https://meet.google.com/cig-khgy-bed |
+------+-----+--------------------------------------+

Cronograma
----------

.. csv-table::
   :header: "Previsão", "Aulas", "Conteúdo"
   :widths: 4, 5, 20
   :file: nc_eng.csv
