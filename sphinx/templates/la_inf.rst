Álgebra Linear
==============

Apresentação
------------

+----------------+-----------------+----------------+-------------+
| **Curso**      | Sistemas de Informação                         |
+----------------+-----------------+----------------+-------------+
| **Disciplina** | Álgebra Linear  | **Modalidade** | Obrigatória |
+----------------+-----------------+----------------+-------------+
| **Código**     |                 | **Requisito**  |             |
+----------------+-----------------+----------------+-------------+
| **Carga**      | **À Distância** | **Teórica**    | 0h          |
| **Horária**    +-----------------+----------------+-------------+
|                | **Presencial**  | **Prática**    | 0h          |
|                |                 +----------------+-------------+
|                |                 | **Teórica**    | 68h         |
+----------------+-----------------+----------------+-------------+

Disciplina ministrada por *Marcelo Barboza* em {{ semestre }}.

Ementa
------

Álgebra Matricial. Sistemas de Equações Lineares. Espaços Vetoriais e
Transformações Lineares, Ortogonalidade e Projeções.

Objetivos
---------

Álgebra Linear é tema vivo de estudos, de central importância para quase todas
as outras áreas da Matemática, tanto pura quanto aplicada, assim como para
Ciências da Computação, para as ciências físicas, biológicas, sociais, e
engenharias. Ela abarca um extenso corpo de resultados teóricos e se presta a
oferecer um vasto e sempre crescente conjunto de técnicas computacionais dos
mais variados matizes.

Geral
~~~~~

Resolver a sistemas de equações lineares à luz dos elementos básicos da Álgebra
Linear, que são os espaços vetoriais e as transformações lineares.

Específico
~~~~~~~~~~

Reconhecer a espaços vetoriais, bases, transformações lineares, produtos
internos e matrizes, bem como resolver a sistemas lineares de equações
algébricas sob a ótica dos elementos acima elencados.

Programa
--------

#. Equações lineares e matrizes:

   * matrizes;
   * álgebra matricial;
   * tópicos matriciais:

     - eliminação;
     - decomposições qr, lu, de Cholesky;
     - determinantes;
     - polinômio característico;
     - autovalores e autovetores;

   * soluções de sistemas de equações lineares;

#. Espaços vetoriais reais:

   * espaços e subespaços vetoriais;
   * independência linear, geradores;
   * base, dimensão;
   * soma direta, projeção;

#. Transformações lineares:

   * transformações lineares;
   * produtos de transformações lineares;
   * núcleo e imagem;
   * matriz de uma transformação linear;

#. Espaços com produto interno:

   * produto interno;
   * adjunta, operadores autoadjuntos;
   * ortogonalidade, projeção ortogonal;
   * bases ortonormais;
   * algoritmo de Gram-Schmidt.

Metodologia
-----------

As aulas terão caráter teórico-expositivo e se darão de forma síncrona através
do Google Meet. O Professor utilizará de mesa-digitalizadora para a explanação
de conteúdos. É essencial que os alunos leiam o livro texto e resolvam o maior
número possível de exercícios.

Avaliação
---------

A avaliação de cada estudante se dará pela percepção do Professor acerca de sua
evolução no decorrer da disciplina. Para tanto, serão avaliadas sua participação
no fórum de discussões do Moodle, nos encontros síncronos via Google Meet e,
finalmente, sua habilidade em resolver aos exercícios propostos.

A nota final :math:`N_f` será assim composta:

#. seja :math:`M` a menção atribuída ao estudante pelo Professor;

#. se :math:`L` a média das notas obtidas pelo estudante em listas de
   exercícios, isto é, se :math:`p` é o número de listas de exercícios no
   decorrer de toda a disciplina, então

   .. math:: L = \dfrac{L_1+\cdots+L_p}{p},

   onde :math:`L_i` representa a nota do aluno em sua lista de excercícios de
   número :math:`i\in\{1,2,\ldots,p\}`;

#. seja :math:`P` a média das notas obtidas pelo estudante em suas duas
   avaliações escritas, ou seja,

   .. math:: P=\dfrac{P_1+P_2}{2},

   onde :math:`P_i` representa a nota do aluno em sua avaliação de número
   :math:`i\in\{1,2\}`,

então:

.. math:: N_f=\dfrac{2M+L+P}{4};

Aprovação por média, reprovação por média e recuperação obedecem, enfim, ao
seguinte critério:

+-----------------------+--------------------------+----------------------+
| Aprovação por média   | Recuperação              | Reprovação por média |
+=======================+==========================+======================+
| :math:`N_f\geqslant6` | :math:`3\leqslant N_f<6` | :math:`N_f<3`        |
+-----------------------+--------------------------+----------------------+

Recuperação
-----------

A cada estudante em recuperação será atribuída uma tarefa cuja execução dependa
de conteúdos aprendidos na disciplina. Se :math:`R` é a nota que corresponde a
este trabalho, então a média final :math:`M_f` do estudante será assim
calculada:

.. math:: M_f=\mbox{max}\left\{N,\dfrac{N+R}{2}\right\}.

Aprovação e reprovação obedecem ao seguinte critério:

+-----------------------+---------------+
| Aprovação             | Reprovação    |
+=======================+===============+
| :math:`M_f\geqslant6` | :math:`M_f<6` |
+-----------------------+---------------+

Bibliografia
------------

.. bibliography:: ../referencias/la_inf.bib
   :style: alpha
   :all:

Material Complementar
---------------------

Vídeos do IMPA:

    - `Jorge Passamani Zubelli <https://goo.gl/D78un1>`_;

Sites interessantes:

    - https://math.stackexchange.com/questions/tagged/linear-algebra;

Formato das aulas
-----------------

+-------------------------+----------------------+
| Atividade               | Tempo médio previsto |
+=========================+======================+
| Leitura do livro texto  | 20 min               |
+-------------------------+----------------------+
| Resolução de exercícios | 20 min               |
+-------------------------+----------------------+
| Vídeos                  | 35 min               |
+-------------------------+----------------------+
| Aula síncrona           | 90 min               |
+-------------------------+----------------------+

Cronograma
----------

.. csv-table::
   :header: "Previsão", "Aulas", "Conteúdo"
   :widths: 4, 5, 20
   :file: la_inf.csv
