Fundamentos de Análise
======================

Apresentação
------------

+----------------+------------------------+----------------+-------------+
| **Curso**      | Licenciatura em Matemática                            |
+----------------+------------------------+----------------+-------------+
| **Disciplina** | Fundamentos de Análise | **Modalidade** | Obrigatória |
+----------------+------------------------+----------------+-------------+
| **Código**     | MAT801                 | **Requisito**  | MAT201      |
+----------------+------------------------+----------------+-------------+
| **Carga**      | **À Distância**        | **Teórica**    | 0h          |
| **Horária**    +------------------------+----------------+-------------+
|                | **Presencial**         | **Prática**    | 0h          |
|                |                        +----------------+-------------+
|                |                        | **Teórica**    | 102h        |
+----------------+------------------------+----------------+-------------+

Disciplina ministrada por *Marcelo Barboza* em {{ semestre }}.

Ementa
------

Números reais. Conjuntos enumeráveis, sequências e séries numéricas. Noções
topológicas da reta. Funções reais, limite e continuidade. Derivada e suas
aplicações.

Objetivos
---------

Esta é uma introdução à Análise Matemática através do estudo das funções reais
de uma variável real.

Geral
~~~~~

Consolidar o conceito de limite na mente dos futuros Professores de Matemática,
sem o qual torna-se improvável que se tenha razoável compreensão acerca dos
números reais.

Específico
~~~~~~~~~~

Estudar conjuntos enumeráveis, sequências e séries de números de números reais,
topologia da reta real, limites de funções, continuidade de funções, derivadas
de funções e algumas de suas várias aplicações.

Programa
--------

#. Conjuntos enumeráveis e não enumeráveis:

   - definição e exemplos;
   - propriedades;

#. Números reais:

   - corpos;
   - corpos ordenados;
   - números reais;

#. Sequências e séries de números reais:

   - sequências e seus limites;
   - propriedades aritméticas dos limites;
   - subsequências;
   - sequências de Cauchy;
   - limites infinitos;
   - séries numéricas;
   - critérios de convergência para séries numéricas;

#. Topologia da Reta:

   - conjuntos abertos;
   - conjuntos fechados;
   - pontos de acumulação;
   - conjuntos compactos;

#. Limite de funções:

   - limites de funções;
   - limites laterais;
   - limites no infinito;

#. Funções contínuas:

   - funções contínuas;
   - funções contínuas em intervalos;
   - funções contínuas em conjuntos compactos;
   - continuidade uniforme;

#. Derivadas:

   - derivada de uma função;
   - funções derivaveis num intervalo;
   - fórmula de Taylor;
   - série de Taylor;

Metodologia
-----------

As aulas terão caráter teórico-expositivo e se darão de forma síncrona através
do Google Meet. O Professor utilizará de mesa-digitalizadora para a explanação
de conteúdos. É essencial que os alunos leiam o livro texto e resolvam o maior
número possível de exercícios.

Avaliação
---------

A avaliação de cada estudante se dará pela percepção do Professor acerca de sua
evolução no decorrer da disciplina. Para tanto, serão avaliadas sua participação
no fórum de discussões do Moodle, nos encontros síncronos via Google Meet e,
finalmente, sua habilidade em resolver aos exercícios propostos.

A nota final :math:`N_f` será assim composta:

#. seja :math:`M` a menção atribuída ao estudante pelo Professor;

#. se :math:`L` a média das notas obtidas pelo estudante em listas de
   exercícios, isto é, se :math:`p` é o número de listas de exercícios no
   decorrer de toda a disciplina, então

   .. math:: L = \dfrac{L_1+\cdots+L_p}{p},

   onde :math:`L_i` representa a nota do aluno em sua lista de excercícios de
   número :math:`i\in\{1,2,\ldots,p\}`;

#. seja :math:`P` a média das notas obtidas pelo estudante em suas duas
   avaliações escritas, ou seja,

   .. math:: P=\dfrac{P_1+P_2}{2},

   onde :math:`P_i` representa a nota do aluno em sua avaliação de número
   :math:`i\in\{1,2\}`,

então:

.. math:: N_f=\dfrac{2M+L+P}{4};

Aprovação por média, reprovação por média e recuperação obedecem, enfim, ao
seguinte critério:

+-----------------------+--------------------------+----------------------+
| Aprovação por média   | Recuperação              | Reprovação por média |
+=======================+==========================+======================+
| :math:`N_f\geqslant6` | :math:`3\leqslant N_f<6` | :math:`N_f<3`        |
+-----------------------+--------------------------+----------------------+

Recuperação
-----------

A cada estudante em recuperação será atribuída uma tarefa cuja execução dependa
de conteúdos aprendidos na disciplina. Se :math:`R` é a nota que corresponde a
este trabalho, então a média final :math:`M_f` do estudante será assim
calculada:

.. math:: M_f=\mbox{max}\left\{N_f,\dfrac{N_f+R}{2}\right\}.

Aprovação e reprovação obedecem ao seguinte critério:

+-----------------------+---------------+
| Aprovação             | Reprovação    |
+=======================+===============+
| :math:`M_f\geqslant6` | :math:`M_f<6` |
+-----------------------+---------------+

Bibliografia
------------

.. bibliography:: ../referencias/ra_mat.bib
   :style: alpha
   :all:

Material Complementar
---------------------

Vídeos do IMPA:

    - `Elon Lages Lima <https://goo.gl/X8BGKH>`_;
    - `Carlos Gustavo Moreira <https://goo.gl/ij474B>`_.

Sites interessantes:

    - https://math.stackexchange.com/questions/tagged/real-analysis;
    - http://www.mathcs.org/analysis/reals.

Formato das aulas
-----------------

+-------------------------+----------------------+
| Atividade               | Tempo médio previsto |
+=========================+======================+
| Leitura do livro texto  | 20 min               |
+-------------------------+----------------------+
| Resolução de exercícios | 20 min               |
+-------------------------+----------------------+
| Vídeos                  | 35 min               |
+-------------------------+----------------------+
| Aula síncrona           | 90 min               |
+-------------------------+----------------------+

Links para as aulas
-------------------

+------+-----+--------------------------------------+
| 2020 | Qua | https://meet.google.com/tic-fgug-djj |
|      +-----+--------------------------------------+
|      | Sex | https://meet.google.com/vdh-boas-bqg |
+------+-----+--------------------------------------+
| 2021 | Qua | https://meet.google.com/xiv-bchz-rqu |
|      +-----+--------------------------------------+
|      | Sex | https://meet.google.com/pyp-wrke-mjv |
+------+-----+--------------------------------------+

Cronograma
----------

.. csv-table::
   :header: "Previsão", "Aulas", "Conteúdo"
   :widths: 4, 5, 20
   :file: ra_mat.csv
