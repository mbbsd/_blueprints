Iniciação à Lógica
==================

Apresentação
------------

+----------------+-----------------+----------------+-------------+
| **Curso**      | Sistemas de Informação                         |
+----------------+-----------------+----------------+-------------+
| **Disciplina** | Lógica          | **Modalidade** | Obrigatória |
+----------------+-----------------+----------------+-------------+
| **Código**     |                 | **Requisito**  |             |
+----------------+-----------------+----------------+-------------+
| **Carga**      | **À Distância** | **Teórica**    | 68h         |
| **Horária**    +-----------------+----------------+-------------+
|                | **Presencial**  | **Prática**    | 0h          |
|                |                 +----------------+-------------+
|                |                 | **Teórica**    | 0h          |
+----------------+-----------------+----------------+-------------+

Disciplina ministrada por *Marcelo Barboza* em {{ semestre }}.

Justificativa para a alteração da carga horária
-----------------------------------------------

A presente disciplina é, como consta no *projeto pedagógico*, estritamente
presencial. É em razão do isolamento social, necessário em virtude da pandemia
de COVID-19, que ora a ministramos à distância via Google Meet.

Ementa
------

Estruturas lógicas. Lógica de argumentação: analogias, inferências, deduções e
conclusões. Lógica sentencial (ou proposicional). Proposições simples e
compostas. Tabelas-verdade. Equivalências. Leis de De Morgan. Diagramas lógicos.
Lógica de primeira ordem. Princípio de contagem e probabilidade. Operações com
conjuntos. Raciocínio lógico envolvendo probelmas aritméticos, geométricos e
matriciais.

Objetivos
---------

Estruturas discretas têm grande relevância na formação de profissionais da
computação, isto porque poderíamos dizer que matemática discreta é a matemática
da computação. Nela se aprendem, dentre outras coisas, sobre o pensamento
matemático e técnicas de demonstração.

Geral
~~~~~

Unindo temas que de outra forma mais pareceriam uma reunião de tópicos
desconexos, elencamos:

   - a importância do raciocínio lógico;
   - o poder da notação matemática;
   - a utilidade de abstrações.

Específico
~~~~~~~~~~

Estudar:

   - lógica de primeira ordem (ou dos predicados);

   - conjuntos:

     * operações entre conjuntos;
     * as leis de De Morgan;
     * enumerabilidade;

   - combinatória:

     * princípios de contagem;
     * permutações;
     * combinações;
     * etc.;

   - uma introdução à probabilidade em espaços amostrais de cardinalidade
     finita;

   - matrizes:

     * sua álgebra;
     * sistemas de equações lineares.

Programa
--------

#. Lógica formal:

   - proposições, representação simbólica e tautologias;
   - lógica proposicional;
   - quantificadores, predicados e validade;
   - lógica dos predicados;

#. Conjuntos, combinatória e probabilidade:

   - conjuntos:

     * relações entre conjuntos;
     * conjuntos de conjuntos;
     * operações unárias e binárias;
     * operações em conjuntos;
     * leis de De Morgan;

   - combinatória:

     * princípio multiplicativo;
     * princípio da casa dos pombos;
     * princípio da inclusão e exclusão;
     * permutações e combinações;
     * permutações e combinações com repetição;

   - probabilidade:

     * introdução à probabilidade discreta;
     * distribuições de probabilidade;
     * probabilidade condicional;
     * esperança;

#. Matrizes:

   - sistemas lineares de equações e matrizes;
   - operações entre matrizes;
   - decomposição lu com pivotação parcial;
   - método das substituições sucessivas e retroativas.

Metodologia
-----------

As aulas terão caráter teórico-expositivo e se darão de forma síncrona através
do Google Meet. O Professor utilizará de mesa-digitalizadora para a explanação
de conteúdos. É essencial que os alunos leiam o livro texto e resolvam o maior
número possível de exercícios.

Avaliação
---------

A avaliação de cada estudante se dará pela percepção do Professor acerca de sua
evolução no decorrer da disciplina. Para tanto, serão avaliadas sua participação
no fórum de discussões do Moodle, nos encontros síncronos via Google Meet e,
finalmente, sua habilidade em resolver a exercícios propostos.

A nota final :math:`N_f` será assim composta:

#. seja :math:`M` a menção atribuída ao estudante pelo Professor;

#. se :math:`L` a média das notas obtidas pelo estudante em listas de
   exercícios, isto é, se :math:`p` é o número de listas de exercícios no
   decorrer de toda a disciplina, então

   .. math:: L = \dfrac{L_1+\cdots+L_p}{p},

   onde :math:`L_i` representa a nota do aluno em sua lista de exercícios de
   número :math:`i\in\{1,2,\ldots,p\}`;

#. seja :math:`P` a média das notas obtidas pelo estudante em suas duas
   avaliações escritas, ou seja,

   .. math:: P=\dfrac{P_1+P_2}{2},

   onde :math:`P_i` representa a nota do aluno em sua avaliação de número
   :math:`i\in\{1,2\}`,

então:

.. math:: N_f=\dfrac{2M+L+P}{4};

Aprovação por média, reprovação por média e recuperação obedecem, enfim, ao
seguinte critério:

+-----------------------+--------------------------+----------------------+
| Aprovação por média   | Recuperação              | Reprovação por média |
+=======================+==========================+======================+
| :math:`N_f\geqslant6` | :math:`3\leqslant N_f<6` | :math:`N_f<3`        |
+-----------------------+--------------------------+----------------------+

Recuperação
-----------

A cada estudante em recuperação será atribuída uma tarefa cuja execução dependa
de conteúdos aprendidos na disciplina. Se :math:`R` é a nota que corresponde a
este trabalho, então a média final :math:`M_f` do estudante será assim
calculada:

.. math:: M_f=\mbox{max}\left\{N_f,\dfrac{N_f+R}{2}\right\}.

Aprovação e reprovação obedecem ao seguinte critério:

+-----------------------+---------------+
| Aprovação             | Reprovação    |
+=======================+===============+
| :math:`M_f\geqslant6` | :math:`M_f<6` |
+-----------------------+---------------+

Bibliografia
------------

.. bibliography:: ../referencias/lo_inf.bib
   :style: alpha
   :all:

Material Complementar
---------------------

Vídeos do Professor Dr. Trefor Bazett no `YouTube <https://youtube.com>`_ :

    - `playlist completa <https://www.youtube.com/watch?v=rdXw7Ps9vxc&list=PLHX
      Z9OQGMqxersk8fUxiUMSIx0DBqsKZS>`_;

Tags do `StackExchange <https://cs.stackexchange.com>`_:

    - https://cs.stackexchange.com/questions/tagged/combinatorics;
    - https://cs.stackexchange.com/questions/tagged/discrete-mathematics;
    - https://cs.stackexchange.com/questions/tagged/logic;
    - https://cs.stackexchange.com/questions/tagged/probability-theory;
    - https://cs.stackexchange.com/questions/tagged/sets;

Formato das aulas
-----------------

+-------------------------+----------------------+
| Atividade               | Tempo médio previsto |
+=========================+======================+
| Leitura do livro texto  | 20 min               |
+-------------------------+----------------------+
| Resolução de exercícios | 20 min               |
+-------------------------+----------------------+
| Vídeos                  | 35 min               |
+-------------------------+----------------------+
| Aula síncrona           | 90 min               |
+-------------------------+----------------------+

Links para as aulas
-------------------

+------+-----+--------------------------------------+
| 2020 | Ter | https://meet.google.com/yhu-qiuj-hzq |
|      +-----+--------------------------------------+
|      | Sex | https://meet.google.com/mub-pept-cnt |
+------+-----+--------------------------------------+
| 2021 | Ter | https://meet.google.com/iqr-uufp-dax |
|      +-----+--------------------------------------+
|      | Sex | https://meet.google.com/cff-hmze-fyp |
+------+-----+--------------------------------------+

Cronograma
----------

.. csv-table::
   :header: "Previsão", "Aulas", "Conteúdo"
   :widths: 4, 5, 20
   :file: lo_inf.csv
