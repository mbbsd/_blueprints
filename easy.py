#!/usr/bin/env python3

from datetime import date
from selenium.webdriver import Firefox
from selenium.webdriver.firefox.options import Options
from pkgs.timeline import blueprint
from pkgs.maneuver import academic

payload = {
        'opening': date(2020, 11, 16),
        'mastery': {
            'lo_inf': {
                'blueprint': blueprint.lo_inf,
                'frequency': {1: 1, 4: 1},
                'adjoining': 3,
                'commonlog': '139572',
                },
            # 'nc_eng': {
            #     'blueprint': blueprint.nc_eng,
            #     'frequency': {0: 1, 2: 1},
            #     'adjoining': 3,
            #     'commonlog': '152741'
            #     },
            # 'ra_mat': {
            #     'blueprint': blueprint.ra_mat,
            #     'frequency': {2: 2, 4: 1},
            #     'adjoining': 3,
            #     'commonlog': '152848'
            #     },
            }
        }


def main():
    # FF options
    options = Options()
    options.headless = False
    # FF instance
    browser = Firefox(options=options)
    browser.implicitly_wait(15)
    # q-academico session
    bookish = academic(browser, payload)
    bookish.login()
    # bookish.change_semester()
    # print(bookish.logbooks())
    # bookish.contents()
    # bookish.register()
    bookish.exams()
    bookish.grades()
    bookish.quit()


if __name__ == '__main__':
    main()
