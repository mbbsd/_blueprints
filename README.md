# README #

This README would normally document whatever steps are necessary to get your
application up and running.

### Blueprints ###

Blueprints foi criado para automatizar tarefas mecânicas inerentes à vida de
professores. Como exemplo, podemos citar:

* planos de ensino;
* lancar conteudo;
* lancar avaliacoes;
* expanding;

### How do I get set up? ###

You'll need the following softwares in order to get *blueprints* up and running:

* Python3;
* Firefox + Geckodriver;

Clone the repository. That *make* command will create a Python's virtualenv
inside it and also install the needed dependencies using pip.

```
$ git clone https://bitbucket.org/mbbsd/_blueprints.git ~/projects/blueprints
$ cd ~/projects/blueprints
$ make venv
```

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* [Marcelo Barboza](marcelo.barboza@ifgoiano.edu.br)
* [Hugo Belisário](hugo@ifg.edu.br)
